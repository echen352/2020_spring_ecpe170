// connect5.c
// Connect-Four (Five in a row edition)

#include <stdlib.h>
#include <stdio.h>

// Board dimensions
#define COLS 9
#define ROWS 6

// Game pieces
#define HUMAN 'H'
#define COMPUTER 'C'
#define EMPTY '.'

// Allowed moves
#define MIN_COL 1
#define MAX_COL 7
#define PLAYABLE_COLS 7
#define MAX_MOVES (ROWS * (COLS - 2)) // 42 possible moves

// num pieces in a row (vertical, horizontal, diagonal) to win
#define NUM_WIN 5

void initRandom(void);
unsigned long int getRandom(void);
void printBoard(void);
int getColHeight(int col);
int playerMove(void);
int checkBoard(int col);

// variables for random number generation
unsigned long int mW = 22222202;
unsigned long int mZ = 99999909;

// Creates the board from column to row
char board[COLS][ROWS] = {
    {'H', 'C', 'H', 'C', 'H', 'C'}, // 1st column
    {'.', '.', '.', '.', '.', '.'}, // 2nd column
    {'.', '.', '.', '.', '.', '.'}, // 3rd column
    {'.', '.', '.', '.', '.', '.'}, // 4th column
    {'.', '.', '.', '.', '.', '.'}, // 5th column
    {'.', '.', '.', '.', '.', '.'}, // 6th column
    {'.', '.', '.', '.', '.', '.'}, // 7th column
    {'.', '.', '.', '.', '.', '.'}, // 8th column
    {'C', 'H', 'C', 'H', 'C', 'H'}  // 9th column
};

int main(void) {
    char piece; // stores game pieces 'H','C','.'
    int col; // stores the index of the column of the last move made
    int moves; // total number of moves made

    // print welcome message
    printf("Welcome to Connect-Four, Five-in-a-Row variant!\n"
           "Version 1.0\n"
           "Implemented by Ellis Chen\n");

    initRandom(); // run the random variable generator
    printf("\nHuman Player (H)\nComputer Player (C)\n");
    
    // choose who goes first at random
    piece = (((getRandom() & 1) ? HUMAN : COMPUTER));
    if (piece == COMPUTER) {
        printf("\nFlipping coin... COMPUTER goes first\n\n");
    } else {
        printf("\nFlipping coin... HUMAN goes first\n\n");
    }

    printBoard(); // print the board

    // loop the game until game detects a win
    for (moves = 0; moves < MAX_MOVES; moves++) {
        if (piece == COMPUTER) {
            // computer makes move
            do {
                col = MIN_COL + (getRandom() % PLAYABLE_COLS); // stores randomized column
            } while (getColHeight(col) == ROWS);
            printf("Computer selected column %d\n\n", col);
        }
        else {
            col = playerMove(); // gets the column from human
        }
        
        // make move
        board[col][getColHeight(col)] = piece;
        
        // update board
        printBoard();
        
        // check for win
        if (checkBoard(col)) {
            break;
        }
        // change player
        piece = (piece == HUMAN ? COMPUTER : HUMAN);
    }
    
    // decides who wins
    if (moves == MAX_MOVES) {
        printf("\nIt's a draw!\n\n");
    } else if (piece == COMPUTER) {
        printf("\nComputer won!\n\n");
    } else {
        printf("\nYou won!\n\n");
    }

    return EXIT_SUCCESS;
}


/* initRandom
   prompts user 2 values to initilaze random number generation
   Parameters: None
   Returns: Nothing
*/
void initRandom(void) {
    printf("Enter two positive numbers to initialize the random number generator.\n");
    printf("Number 1: ");
    scanf("%lu", &mW);
    printf("Number 2: ");
    scanf("%lu", &mZ);
    return;
}


/* getRandom
   Generates random number
   Parameters: None
   Returns: Unsigned 32-bit integer.
*/
unsigned long int getRandom(void) {
    mZ = 36969 * (mZ & 65535) + (mZ >> 16);
    mW = 18000 * (mW & 65535) + (mW >> 16);
    return (mZ << 16) + mW;
}


/* getColHeight
   Returns the number of pieces in column
   Parameters: col - column number of last move
   Returns: num pieces in column
*/
int getColHeight(int col) {
    int height = 0;
    while (height < ROWS) {
        if (board[col][height] == EMPTY) {
            break;
        }
        height++;
    }
    return height;
}

/* printBoard
   prints updated boards
   Parameters: none
   Returns: none
*/
void printBoard(void) {
    printf("\n   1 2 3 4 5 6 7\n -----------------\n");
    for (int row = ROWS - 1; row >= 0; row--) {
        for (int col = 0; col < COLS; col++) {
            printf(" %c", board[col][row]);
        }
        printf("\n");
    }
    printf(" -----------------\n\n");
}

/* playerMove
   Prompts player to pick a column
   Parameters: none
   Returns: Column number for player piece
*/
int playerMove(void) {
    int col = 0;

    while (col == 0) {
        printf("What column would you like to drop token into? Enter 1-7: ");
        scanf("%d", &col);
        if (col < MIN_COL || col > MAX_COL) {
            printf("Out of range! Try another column.\n");
            col = 0;
        } else if (getColHeight(col) == ROWS) {
            printf("That column is full! Try another column.\n");
            col = 0;
        }
    }

    return col;
}

/* checkBoard
   Checks for win after last move
   Parameters: col - the column nunmber of last move
   Returns: 1 if last winning move is true, 0 otherwise
*/
int checkBoard(int col) {
    /* Get row coordinate of last move and the kind of stone */
    const int row = getColHeight(col) - 1;
    const char piece = board[col][row];

    // Check vertically
    int count = 1;
    for (int i = row - 1; i >= 0; i--) {
        if (board[col][i] != piece) {
           break;
        }
        count++;
    }
    if (count >= NUM_WIN) {
        return 1;
    }

    // Check horizontally
    count = 1;
    for (int j = col - 1; j >= 0; j--) {
        if (board[j][row] != piece) {
            break;
        }
        count++;
    }
    for (int j = col + 1; j < COLS; j++) {
        if (board[j][row] != piece) {
            break;
        }
        count++;
    }
    if (count >= NUM_WIN) {
        return 1;
    }

    // Check diagonally from left top corner to right bottom corner
    count = 1;
    for (int i = row + 1, j = col - 1; i < ROWS && j >= 0; i++, j--) {
        if (board[j][i] != piece) {
            break;
        }
        count++;
    }
    for (int i = row - 1, j = col + 1; i >= 0 && j < COLS; i--, j++) {
        if (board[j][i] != piece) {
            break;
        }
        count++;
    }
    if (count >= NUM_WIN) {
        return 1;
    }

    // Check diagonally from left bottom corner to right top corner
    count = 1;
    for (int i = row - 1, j = col - 1; i >= 0 && j >= 0; i--, j--) {
        if (board[j][i] != piece) {
            break;
        }
        count++;
    }
    for (int i = row + 1, j = col + 1; i < ROWS && j < COLS; i++, j++) {
        if (board[j][i] != piece) {
            break;
        }
        count++;
    }
    if (count >= NUM_WIN) {
        return 1;
    }

    // no winner
    return 0;
}

