# Ellis Chen
# Connect4 (5-in-a-row variant)

EMPTY = 46 # ASCII code for '.' (empty cells)
HUMAN = 72 # ASCII code for 'H' (humna piece)
COMPUTER = 67 # ASCII code for 'C' (computer piece)
SPACE =	32 # ASCII code for space ' '
NEWLINE = 10 # ASCII code for '\n'

COLS = 9 # Total number of columns on board
PLAYABLE_COLS = 7 # Number of playable columns on board
ROWS = 6 # Total number of rows on board
MIN_COL = 1 # Index of first playable column
MAX_COL = 7 # Index of last playable column
MAX_MOVES = 42 # Maximum number of moves
WIN_NUM = 5 # Number of like pieces in a row to win
	
.text

.globl	main
main:
	# Register map
	# $s0 = player piece 'H', 'C'
	# $s1 = keeps count of total moves made
	# $s2 = stores the column number to make move

	# Welcome message
	li $v0, 4 # syscall for print string
	la $a0, msgWelcome # print welcoming message
	syscall	# print string

	# Initialize random number generator
	jal initRandom # Call to initialize random number generation

	# Randomly choose first player
	jal getRandom # $ stores random number inot $v0
	andi $v0, $v0, 1 # Get lowest bit of random number
	beqz $v0, mainFirstComputer # if random number = 0, copmuter moves first
	
mainFirstHuman:
	li $s0, HUMAN # set piece to be 'H'
	li $v0, 4 # syscall for print string
	la $a0, msgFirstHuman # print human goes first
	syscall	# Print string
	j mainFirstPlayerChosen # Proceed to print board
	
mainFirstComputer:
	li $s0, COMPUTER # set piece to be 'C'
	li $v0, 4 # syscall for print string
	la $a0, msgFirstComputer # print copmuter goes first
	syscall	 # Print string
	
mainFirstPlayerChosen:

	# Print board
	jal printBoard # Call to print board

	# Loop the game every turn
	move $s1, $zero # total num of moves = 0
mainLoop:
	beq $s1, MAX_MOVES, mainExitLoop # No more empty cells, exit loop

	# Decides who goes first
	beq $s0, HUMAN, mainHumanTurn # If the piece = 'H', human's turn
	
	# Computer's turn
mainComputerTurn:
	jal getRandom # call to get random number, returns num to $v0
	li $t0, PLAYABLE_COLS # loads number of playable columns left
	rem $t0, $v0, $t0 # stores (random % playable columns)
	addi $s2, $t0, MIN_COL # stores column number = MIN_COL + (random % playable columns)
	move $a0, $s2 # stores colmumn number as arguement for getColHeight
	jal getColHeight # returns the height of the column into $v0
	beq $v0, ROWS, mainComputerTurn # compares height to number of rows, loops if column is full
	
	# Reports the move made by computer
	li $v0, 4 # syscall for print string
	la $a0, msgComputerMove # prints that computer has made a move
	syscall	# Print string
	
	li $v0, 1 # syscall for print integer
	move $a0, $s2 # prints the column number
	syscall # Print integer
	
	li $v0, 11 # syscall for print char
	li $a0, NEWLINE # prints the line feed
	syscall # Prints new line
	syscall # Prints it again
	
	j mainPlacePiece # Jump to section to place pieces
	
	# Human player makes move
mainHumanTurn:
	jal playerMove # stores column selected by human into $v0
	move $s2, $v0 # stores column in $s2
	
	# Put stone in selected column
mainPlacePiece:
	move $a0, $s2 # stores column number as arugment to getColHeight
	jal getColHeight # returns height of column into $v0
	li $t0, ROWS # loads number of rows
	mul $t0, $t0, $s2 # $ gets the offset of the column (ROWS * col)
	add $t0, $t0, $v0 # $ adds the height to the offset (ROWS * col + height)
	sb $s0, board($t0) # board[col][height] = piece
	
	jal printBoard # Call to print updated board
	
	# Checks for winning condition
	move $a0, $s2 # stores column number into arguement for checkBoard
	jal checkBoard # returns 1 if winning condition is true, else 0, stored into $v0
	bnez $v0, mainExitLoop # End the loop to finish game if winning condition is true

	# switch player turn
	addi $s1, $s1, 1 # adds to total moves made
	beq $s0, COMPUTER, mainHumanSwitchTurn # If computer played last, now player's turn
	
mainComputerSwitchTurn:
	li $s0, COMPUTER # set piece to 'C'
	j mainLoop # loop game
	
mainHumanSwitchTurn:
	li $s0, HUMAN # set piece to 'H'
	j mainLoop # loop game
	
mainExitLoop:
	# checks who won
	beq $s1, MAX_MOVES, main_draw # checks if board is full, results in a draw
	beq $s0, COMPUTER, mainComputerWin # if winning move is computer, computer wins
	
mainHumanWin:
	li $v0, 4 # syscall for print string
	la $a0, msgWin # prints human won
	syscall # Print string
	j mainEndProgram # end program
	
mainComputerWin:
	li $v0, 4 # syscall for print string
	la $a0, msgLose # prints computer won
	syscall	# Print string
	j mainEndProgram # end program
	
main_draw:
	li $v0, 4 # syscall for print string
	la $a0, msgDraw # prints the game is a draw
	syscall	# Print string

mainEndProgram:
	li $v0, 10 # syscall for exit program
	syscall	# end program
	
# ------------------------------------------------------------------
	
	# Function: void initRandom(void)
	# Arguments: none
	# Returns: nothing
	# prompt user to enter 2 integers for random number generation
initRandom:

	# Asks the user for 2 integers
	li $v0, 4 # syscall for print string
	la $a0, msgInitRandomPrompt # print prompt
	syscall	# print string
	
	# Ask user for 1st integer
	li $v0, 4 # syscall for print string
	la $a0, msgInitRandom1	# print request for 1st integer
	syscall	# print string
	
	li $v0, 5 # syscall for read int
	syscall	# Read integer, result is stored into $v0
	sw $v0, m_w # Save 1st integer
	
	# Ask user for 2nd integer
	li $v0, 4 # syscall for print string
	la $a0, msgInitRandom2	# print request for 2nd integer
	syscall	# print string
	
	li $v0, 5 # syscall for read int
	syscall	# Read integer, result is stored into $v0
	sw $v0, m_z # Save 2nd integer
	
	# Return to main
	jr $ra # Jump to address stored in $ra

# ------------------------------------------------------------------
	
	# Function: unsigned int getRandom(void)
	# Arguments: none
	# Returns: result stored in $v0
getRandom:

	# Update 1st state variable into $t0
	lw $t0, m_z # Load 1st state var into $t0
	andi $t1, $t0, 65535 # m_z & 65535
	li $t2, 36969 # 36969
	multu $t1, $t2 # 36969 * (m_z & 65535)
	mflo $t1 # $t1 := 36969 * (m_z & 65535)
	srl $t0, $t0, 16 # m_z >> 16
	addu $t0, $t0, $t1 # $t0 = 36969 * (m_z & 65535) + (m_z >> 16)
	sw $t0, m_z # Save 1st state variable
	
	# Update 2nd state variable
	lw $t1, m_w # Load 2nd state variable into $t1
	andi $t2, $t1, 65535 # m_w & 65535
	li $t3, 18000 # 18000
	multu $t2, $t3 # 18000 * (m_w & 65535)
	mflo $t2 # $t2 := 18000 * (m_w & 65535)
	srl $t1, $t1, 16 # m_w >> 16
	addu $t1, $t1, $t2 # $t1 = 18000 * (m_w & 65535) + (m_w >> 16)
	sw $t1, m_w # Save 2nd state variable
	
	# generate random number
	sll $v0, $t0, 16 # m_z << 16
	addu $v0, $v0, $t1 # result = (m_z << 16) + m_w
	
	# Return to main
	jr $ra # Jump to address stored in $ra
	
# ------------------------------------------------------------------
	
	# Function: int getColHeight(int col)
	# Arguments: $a0 = column number
	# Returns: height of column stored in $v0
getColHeight:
	# Loop over column
	add $v0, $zero, $zero # $ set row = 0
	mul $t0, $a0, ROWS # gets the offset of cell at [row][col]
	
getColHeightLoop:
	beq $v0, ROWS, getColHeightEndLoop # exit loop if column is full
	lbu $t1, board($t0) # loads board[col][row]
	beq $t1, EMPTY, getColHeightEndLoop # exit loop if current cell is empty
	addi $t0, $t0, 1 # increment the offset
	addi $v0, $v0, 1 # increment the row
	j getColHeightLoop # loop

getColHeightEndLoop:
	jr $ra # jump back to main

# ------------------------------------------------------------------
	
	# FUNCTION: int checkBoard(int col)
	# Arguments: $a0 = column number of last move
	# Returns: 1 if last move results in a win, 0 otherwise, stored in $v0
checkBoard:

	# Register map:
	# $a0: col - the current column
	# $t0: offset of cell at board[col][row]
	# $t1: row - the current row
	# $t2: offset of cell at [j][i] in loops
	# $t3: contains 1 if piece exists in cell, 0 otherwise
	# $t4: i - index of row in loops
	# $t5: j - index of column in loops
	# $t6: count - keeps track of how many like pieces are connected at once
	# $t7: cell type of top piece

	# Save return address on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $ra, 0($sp) # Store return address on stack

	# Gets the offset of the top piece in the selected column
	jal getColHeight # height of column col is stored into $v0
	addi $t1, $v0, -1 # row = height - 1
	mul $t0, $a0, ROWS # offset of column
	add $t0, $t0, $t1 # offset of top piece
	lbu $t7, board($t0) # loads piece at board[col][row]
	
	# Check the board vertically
	li $t6, 1 # sets count to 1
	addi $t4, $t1, -1 # sets i to be index of row = row - 1
	addi $t2, $t0, -1 # sets the offset of cell at [col][i]
	
checkBoardVerticalLoop:
	bltz $t4, checkBoardVerticalEndLoop # if i < 0, then end loop
	lbu $t3, board($t2) # board[col][i]
	seq $t3, $t3, $t7 # checks if cell has a piece (board[col][i] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[col][i] == piece), then count++
	addi $t4, $t4, -1 # i--
	addi $t2, $t2, -1 # update $t2 to offset of cell at [col][i]
	bnez $t3, checkBoardVerticalLoop # loop back if cell was not empty
	
checkBoardVerticalEndLoop:
	# Check if there are enough pieces stacked vertically
	blt $t6, WIN_NUM, checkBoardHorizontal	# If true, continue to check horizontally
	li $v0, 1 # Return value 1 means winning move is detected
	j checkBoardEnd # End checkBoard function
	
	# Check board horizotally
checkBoardHorizontal:
	li $t6, 1 # sets count = 1
	addi $t5, $a0, -1 # set j to be index of col = col - 1
	sub $t2, $t0, ROWS # get the offset of cell at [j][row]
	
checkBoardHorizontalLeftLoop:
	bltz $t5, checkBoardHorizontalLeftEndLoop # if j < 0, then end loop
	lbu $t3, board($t2) # board[j][row]
	seq $t3, $t3, $t7 # checks if cell has a piece (board[j][row] ==  piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece), then count++
	addi $t5, $t5, -1 # j--
	sub $t2, $t2, ROWS # update $t2 to offset of cell at [j][row]
	bnez $t3, checkBoardHorizontalLeftLoop # loop horizontal left
	
checkBoardHorizontalLeftEndLoop:
	addi $t5, $a0, 1 # j = col + 1
	addi $t2, $t0, ROWS # gets offset of cell at [j][row]
	
checkBoardHorizontalRightLoop:
	bge $t5, COLS, checkBoardHorizontalRightEndLoop # if j >= COLS, then end loop
	lbu $t3, board($t2) # board[j][row]
	seq $t3, $t3, $t7 # $ checks if cell has a piece (board[j][row] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece) then count++
	addi $t5, $t5, 1 # j++
	addi $t2, $t2, ROWS # update $t2 to offset of cell at [j][row]
	bnez $t3, checkBoardHorizontalRightLoop # loop horizontal right
	
checkBoardHorizontalRightEndLoop:
	# Checks to see if there are enough like pieces in a row horizontally
	blt $t6, WIN_NUM, checkBoardDiagonal_1	# If true, continue to check diagonally
	li $v0, 1 # Return value 1 means winning move is detected
	j checkBoardEnd	# End checkBoard function
	
	# Check board diagonally
checkBoardDiagonal_1:
	li $t6, 1 # set count = 1
	addi $t4, $t1, 1 # i = row + 1
	addi $t5, $a0, -1 # j = col - 1
	sub $t2, $t0, ROWS # gets the offset of cell at [j][row]
	addi $t2, $t2, 1 # $gets the offset of cell at [j][i]
	
checkBoardDiagonal_1LeftLoop:
	bge $t4, ROWS, checkBoardDiagonal_1LeftEndLoop # if i >= ROWS, then end loop
	bltz $t5, checkBoardDiagonal_1LeftEndLoop # if c < 0, then end loop
	lbu $t3, board($t2) # board[j][i]
	seq $t3, $t3, $t7 # checks if cells has a piece (board[j][row] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece), then count++
	addi $t5, $t5, -1 # j--
	sub $t2, $t2, ROWS # update $t2 to offset of cell at [j][i]
	addi $t4, $t4, 1 # i++
	addi $t2, $t2, 1 # update $t2 to offset of cell at [j][i]
	bnez $t3, checkBoardDiagonal_1LeftLoop	# loop diagonal_1left
	
checkBoardDiagonal_1LeftEndLoop:
	addi $t4, $t1, -1 # i = row - 1
	addi $t5, $a0, 1 # j = col + 1
	addi $t2, $t0, ROWS # gets the offset of cell at [j][row]
	addi $t2, $t0, -1 # gets the offset of cell at [j][i]
	
checkBoardDiagonal_1RightLoop:
	bltz $t4, checkBoardDiagonal_1RightEndLoop # if i < 0, then end loop
	bge $t5, COLS, checkBoardDiagonal_1RightEndLoop # if j >= COLS, then end loop
	lbu $t3, board($t2) # board[j][row]
	seq $t3, $t3, $t7 # checks if cell has a piece (board[j][row] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece) then count++
	addi $t5, $t5, 1 # j++
	addi $t2, $t2, ROWS # update $t2 to offset of cell at [j][row]
	addi $t4, $t4, -1 # i--
	addi $t2, $t2, -1 # update $t2 to offset of cell at [j][i]
	bnez $t3, checkBoardDiagonal_1RightLoop	# loop diagonal_1right
	
checkBoardDiagonal_1RightEndLoop:
	# Checks if there are enough like pieces in a row to win
	blt $t6, WIN_NUM, checkBoardDiagonal_2 # If true, continue to check diagonal_2
	li $v0, 1 # Return value 1 means winning move is detected
	j checkBoardEnd # End checkBoard function
	
	# Check diagonally in the other direction
checkBoardDiagonal_2:
	li $t6, 1 # set count = 1
	addi $t4, $t1, -1 # i = row - 1
	addi $t5, $a0, -1 # j = col - 1
	sub $t2, $t0, ROWS # gets the offset of cell at [j][row]
	addi $t2, $t2, -1 # gets the offset to cell at [j][i]
	
checkBoardDiagonal_2LeftLoop:
	bltz $t4, checkBoardDiagonal_2LeftEndLoop # if i < 0, then end loop
	bltz $t5, checkBoardDiagonal_2LeftEndLoop # if j < 0, then end loop
	lbu $t3, board($t2) # board[j][i]
	seq $t3, $t3, $t7 # $t3 := (board[j][row] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece) then count++
	addi $t5, $t5, -1 # j--
	sub $t2, $t2, ROWS # update $t2 to offset of cell at [j][i]
	addi $t4, $t4, -1 # i--
	addi $t2, $t2, -1 # update $t2 to offset of cell at [j][i]
	bnez $t3, checkBoardDiagonal_2LeftLoop # loop diagonal_2left
	
checkBoardDiagonal_2LeftEndLoop:
	addi $t4, $t1, 1 # i = row + 1
	addi $t5, $a0, 1 # j = col + 1
	addi $t2, $t0, ROWS # gets the offset of cell at [j][row]
	addi $t2, $t0, 1 # gets the offset of cell at [j][i]
	
checkBoardDiagonal_2RightLoop:
	bge $t4, ROWS, checkBoardDiagonal_2RightEndLoop	# if i >= ROWS, then end loop
	bge $t5, COLS, checkBoardDiagonal_2RightEndLoop # if j >= COLS, then end loop
	lbu $t3, board($t2) # board[j][row]
	seq $t3, $t3, $t7 # checks if cell is empty (board[j][row] == piece ? 1 : 0)
	add $t6, $t6, $t3 # if (board[j][row] == piece) then count++
	addi $t5, $t5, 1 # j++
	addi $t2, $t2, ROWS # update $t2 to offset of cell at [j][row]
	addi $t4, $t4, 1 # i++
	addi $t2, $t2, 1 # update $t2 to offset of cell at [j][i]
	bnez $t3, checkBoardDiagonal_2RightLoop # loop diagonal_2right
	
checkBoardDiagonal_2RightEndLoop:
	# Checks if there are enough like pieces in a row diagonally
	blt $t6, WIN_NUM, checkBoardNoWin # If true, no more checks, no winning move
	li $v0, 1 # else, return value = 1, found winning move
	j checkBoardEnd # End checkBoard function
	
	# No winning condition found, set return value = 0
checkBoardNoWin:
	add $v0, $zero, $zero # store 0 (fail) into $v0

	# Return to main
checkBoardEnd:
	# Restore return address and adjust stack pointer
	lw $ra, 0($sp) # Restore return address from stack
	addi $sp, $sp, 4 # Restore stack pointer
	jr $ra # jump to address stored in $ra
	
# ------------------------------------------------------------------
	
	# Function: int playerMove(void)
	# Arguments: none
	# Returns: selected column in $v0
playerMove:

	# Register map:
	# $s0: user selected column

	# Save return address and $s0 on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $s0, 0($sp) # Store $s0 on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $ra, 0($sp) # Store return address on stack

	add $s0, $zero, $zero # set column = 0
	
playerMoveLoop:
	bnez $s0, playerMoveEnd # column is set - end loop

	# Request user for input
	li $v0, 4 # syscall for print string
	la $a0, msgMoveInput # string to print - request for input
	syscall	# Print string
	li $v0, 5 # syscall for read int
	syscall	# Read integer into $v0
	move $s0, $v0 # store column
	
	# Check for valid range
	blt $s0, MIN_COL, playerMoveRange # column < MIN_COL, then out of range
	bgt $s0, MAX_COL, playerMoveRange # column > MAX_COL, then out of range
	
	# Check if column is full
	move $a0, $s0 # load column as argument for function getColHeight
	jal getColHeight # stores height of column into $v0
	beq $v0, ROWS, playerMoveFull # if column height == ROWS, then the column is full
	
	# Column is in valid range and not full. Return to main
	j playerMoveEnd
	
	# Print out of range error message
playerMoveRange:
	li $v0, 4 # syscall for print string
	la $a0, msgErrorRange # print error message for out of range
	syscall	# Print string
	move $s0, $zero	# set column = 0
	j playerMoveLoop # Repeat loop, check for new input

	# Print column is full error message
playerMoveFull:
	li $v0, 4 # syscall for print string
	la $a0, msgErrorFull # print column is full
	syscall	# Print string
	move $s0, $zero # set column = 0
	j playerMoveLoop # Repeat loop, check for new input
	
	# Return from function
playerMoveEnd:
	move $v0, $s0 # set value = col
	lw $ra, 0($sp) # Restore return address from stack
	addi $sp, $sp, 4 # Restore stack pointer
	lw $s0, 0($sp) # Restore $s0 from stack
	addi $sp, $sp, 4 # Restore stack pointer
	jr $ra # Jump to address stored in $ra

# ------------------------------------------------------------------
	
	# Function: void printBoard(void)
	# Arguments: none
	# Returns: nothing
	# prints the board
printBoard:

	# Register map:
	# $s0: row
	# $s1: col
	# $s2: offset of cell at [col][row]

	# Save registers on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $s2, 0($sp) # Store $s2 on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $s1, 0($sp) # Store $s1 on stack
	addi $sp, $sp, -4 # Adjust stack pointer
	sw $s0, 0($sp) # Store $s0 on stack

	# Print numbers on header
	li $v0, 4 # syscall for print string
	la $a0, boardNumbers # prints board numbers
	syscall	# Print string
	
	# Print top border
	li $v0, 4 # syscall code for print_string
	la $a0, boardDashes # print the board dashes
	syscall	# Print string
	
	# Cycle over rows
	li $s0, ROWS # set row = ROWS
	addi $s0, $s0, -1 # row = ROWS - 1
	
printBoardRowLoop:
	bltz $s0, printBoardRowLoopEnd # if row < 0, then end outer loop

	# loop columns
	add $s1, $zero, $zero # set col = 0
	move $s2, $s0 # stores the offset of cell at [0][row]
printBoardColumnLoop:
	bge $s1, COLS, printBoardColumnLoopEnd	# if col >= COLS, then end inner loop

	# Print cell
	li $v0, 11 # syscall for print char
	li $a0, SPACE # print space char
	syscall	# Print char
	lbu $a0, board($s2) # Load char of cell at [col][row]
	syscall	# Print the piece in cell at board[col][row]

	# Increment column and loop
	addi $s1, $s1, 1 # col++
	addi $s2, $s2, ROWS # Update offset to cell [col][row]
	j printBoardColumnLoop # Repeat inner loop
	
printBoardColumnLoopEnd:

	# Print new line
	li $v0, 11 # syscall for print char
	li $a0, NEWLINE # print new line
	syscall	# Print char

	# Decrement row and loop
	addi $s0, $s0, -1 # row--
	j printBoardRowLoop # Repeat outer loop
	
printBoardRowLoopEnd:

	# Print bottom border
	li $v0, 4 # syscall for print string
	la $a0, boardDashes # print board dashes
	syscall	# Print string
	li $v0, 11 # syscall for print char
	li $a0, SPACE # print space
	syscall	# Print char

	# Restore registers from stack and return to main
	lw $s0, 0($sp) # Restore $s0 from stack
	addi $sp, $sp, 4 # Restore stack pointer
	lw $s1, 0($sp) # Restore $s1 from stack
	addi $sp, $sp, 4 # Restore stack pointer
	lw $s2, 0($sp) # Restore $s2 from stack
	addi $sp, $sp, 4 # Restore stack pointer
	
	jr $ra # Jump to addr stored in $ra

# ------------------------------------------------------------------
.data
	
m_w: .word 22222202 # State variable of random number generator
m_z: .word 99999909 # State variable of random number generator

board: .ascii "HCHCHC..........................................CHCHCH" # Board cells

msgWelcome: .asciiz "Welcome to Connect-Four, Five-in-a-Row variant!\nVersion 1.0\nImplemented by Ellis Chen\n"
msgFirstHuman: .asciiz "Coin toss... HUMAN goes first\n\n"
msgFirstComputer: .asciiz "Coin toss... COMPUTER goes first\n\n"
msgInitRandomPrompt: .asciiz "Enter two positive numbers to initialize the random number generator.\n"
msgInitRandom1: .asciiz "Number 1: "
msgInitRandom2: .asciiz "Number 2: "
msgComputerMove: .asciiz "Computer player selected column "
boardNumbers: .asciiz "\n   1 2 3 4 5 6 7\n"
boardDashes: .asciiz " -----------------\n"
msgMoveInput: .asciiz "What column would you like to drop token into? Enter 1-7: "
msgErrorRange: .asciiz "Out of range! Try another column.\n"
msgErrorFull: .asciiz "That column is full! Try another column.\n"
msgDraw: .asciiz "\nIt's a draw!\n\n"
msgWin: .asciiz "\nYou won!\n\n"
msgLose: .asciiz "\nComputer won!\n\n"

