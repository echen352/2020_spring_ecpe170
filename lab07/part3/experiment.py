#!/usr/bin/env python3
import math
import subprocess
from subprocess import Popen
from subprocess import PIPE
import io
import re

SIZE = [256, 512, 768, 1024, 1280, 1536, 1792, 2048]
result = [["" for i in range(len(SIZE))] for j in range(2)]

out = open("matrix.dat", "w")

def double_print(f):
    print(f)
    out.write(f + "\n")

def parse_output(output):
    f = '(?<=Floating-point ops/sec: ).*'
    o = re.search(f, output)
    return str(int(round(float(o.group(0)) / 1E6)))

def run(aNumber, aSize):
    with Popen(["nice", "-n", "-20", "./matrix_math", str(aNumber), str(aSize)], stdout=PIPE, stderr=PIPE, encoding="UTF-8") as proc:  
        ret = parse_output(proc.communicate()[0])
    return ret

def print_result(result):
    double_print("Floating point operations per second (MFLOPS)")
    head = " \t" + "1 \t"+ "2 \t"    
    double_print(head)

    for i in range(len(SIZE)):
        f = str(SIZE[i]) + "\t" + result[0][i] + "\t" + result[1][i]
        double_print(f)    

for i in range(len(SIZE)):    
    result[0][i] = run(1, SIZE[i])
    result[1][i] = run(2, SIZE[i])

print_result(result)

