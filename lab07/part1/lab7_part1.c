#include <stdint.h>
#include <stdio.h>

#define A 5
#define B 4
#define C 3

// Fills array_2d[5][4]
void fill_2d(uint32_t array[][B]) {
  for (uint32_t a = 0; a < A; ++a) {
    for (uint32_t b = 0; b < B; ++b) {
      array[a][b] = 10 * a + b;
    }
  }
}

// Fills array_3d[5][4][3]
void fill_3d(uint32_t array[][B][C]) {
  for (uint32_t a = 0; a < A; ++a) {
    for (uint32_t b = 0; b < B; ++b) {
      for (uint32_t c = 0; c < C; ++c) {
        array[a][b][c] = 100 * a + 10 * b + c;
      }
    }
  }
}

/* Prints all elements of the 2d array along with their addresses,
and shows that all elements are accessed back to back in memory*/
void print_2d(uint32_t array[][B]) {
  printf("Printing data for 2D array (address: %p)\n\n", (void *)array);
  printf("Array element \t Memory Address \t Distance from previous element (in bytes)\n");

  void *prev = NULL; // memory address of previous element
  int dist = 0;      // distance from previous element in bytes

  printf("****************************************************************\n");
  
  for (uint32_t a = 0; a < A; ++a) {
    for (uint32_t b = 0; b < B; ++b) {
      if (prev != NULL) {
        dist = (void *)&array[a][b] - prev;
      }
      prev = &array[a][b];
      printf("a[%d][%d]=%d \t %p \t %d\n", a, b, array[a][b], &array[a][b], dist);
    }
  }
  printf("****************************************************************\n");
}

/* Prints all elements of the 2d array along with their addresses,
and shows that all elements are accessed back to back in memory */
void print_3d(uint32_t array[][B][C]) {
  printf("Printing data for 3D array (address: %p)\n\n", (void *)array);
  printf("Array element \t Memory Address \t Distance from previous element (in bytes)\n");

  void *prev = NULL; // memory address of previous element
  int dist = 0;      // distance from previous element in bytes

  printf("****************************************************************\n");
  for (uint32_t a = 0; a < A; ++a) {
    for (uint32_t b = 0; b < B; ++b) {
      for (uint32_t c = 0; c < C; ++c) {
        if (prev != NULL) {
          dist = (void *)&array[a][b][c] - prev;
        }
        prev = &array[a][b][c];
        printf("a[%d][%d][%d]=%d \t %p \t %d\n", a, b, c, array[a][b][c], &array[a][b][c], dist);
      }
    }
  }
  printf("****************************************************************\n");
}

int main() {

  uint32_t array_2d[A][B];    // 2D array
  uint32_t array_3d[A][B][C]; // 3D array

  // fill arrays
  fill_2d(array_2d);
  fill_3d(array_3d);

  /* print arrays, their elements, their addresses, 
  and the number of bytes between each element */
  print_2d(array_2d);
  printf("\n");
  print_3d(array_3d);

  return 0;
}

