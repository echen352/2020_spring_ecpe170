#include <stdint.h>
#include <stdio.h>

#define ROWS 2
#define COLS 3

void print_array(int a[ROWS][COLS], int r, int c) {
  printf("a[%d][%d] \t %p \t + %ld \n", r, c, &a[r][c], (void *)&a[r][c] - (void *)a);
}

// Sum array row-by-row
int sumarrayrows(int a[ROWS][COLS]) {
  int i, j, sum = 0;

  for (i = 0; i < ROWS; i++) {
    for (j = 0; j < COLS; j++) {
      sum += a[i][j];
      print_array(a, i, j);
    }
  }

  return sum;
}

// Sum array column-by-column
int sumarraycols(int a[ROWS][COLS]) {
  int i, j, sum = 0;

  for (j = 0; j < COLS; j++) {
    for (i = 0; i < ROWS; i++) {
      sum += a[i][j];
      print_array(a, i, j);
    }
  }

  return sum;
}

int main() {
  int a[ROWS][COLS] = {0};

  printf("sumarrayrows():\n\n");
  printf("Array element \t Memory Address \t Offset (in bytes)\n");
  printf("**********************************************************\n");
  sumarrayrows(a);
  printf("**********************************************************\n");

  printf("\nsumarraycols():\n\n");
  printf("Array element \t Memory Address \t Offset (in bytes)\n");
  printf("**********************************************************\n");
  sumarraycols(a);
  printf("**********************************************************\n");

  return 0;
}

