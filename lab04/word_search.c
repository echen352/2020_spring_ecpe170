// Ellis Chen
// ellischen352@gmail.com

// word_search.c
// Function definitions

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "word_search.h"

int load(const char* fileName, WordSearch *ws, int wordsToFind)
{
    FILE *fp;
    int row, col;
    char * line;

    // Initialize puzzle //
    ws->grid = NULL;
    ws->colNum = 0;
    ws->rowNum = 0;
    ws->numWords = 0;

    // try to open puzzle text file
    fp = fopen(fileName, "r");
    if (!fp) {
        // failed to open source file
        printf("Error: can't open puzzle file %s\n", fileName);
        return 0;
    }

   // read puzzle size
    fscanf(fp, "%d %d %d", &(ws->rowNum), &(ws->colNum), &(ws->numWords));
    
    printf("Game is %d rows X %d cols with %d words\n", ws->rowNum, ws->colNum, ws->numWords);
    wordsToFind = ws->numWords;

    // temporary line for reading from the text file
    line = (char*)malloc(sizeof(char) * ws->colNum + 2);

    // allocate and fill grid
    ws->grid = (char**)malloc(sizeof(char*) * ws->rowNum);
    for (row = 0; row < ws->rowNum; row++) {
        ws->grid[row] = (char*)malloc(sizeof(char) * ws->colNum);
        fscanf(fp, "%s", line);


        // copy all characters from the line to the grid and convert them to lowercase
        for (col = 0; col < ws->colNum; col++) {
            ws->grid[row][col] = tolower(line[col]);
        }
    }
    free(line);

    // close puzzle text file
    fclose(fp);
    return 1;   // puzzle loaded
}




void printPuzzle(WordSearch *ws)
{

    for (int i = 0; i < ws->rowNum; i++) {
        for (int j = 0; j < ws->colNum; j++)
            printf("%c", ws->grid[i][j]);
        printf("\n");
    }
}



void clearPuzzle(WordSearch *ws)
{

    for (int i = 0; i < ws->rowNum; i++) {
        free(ws->grid[i]);
    }
    free(ws->grid);
    ws->rowNum = 0;
    ws->colNum = 0;
}



int searchWord(WordSearch *ws, const char* word)
{

    // look for the direct word in 4 directions
    int found = search4ways(ws, word);

    // create reversed copy of the word
    char* reversed = (char*)malloc(sizeof(char) * (strlen(word) + 1));
    strcpy(reversed, word);
    reverseWord(reversed);

    // look for the reversed word in 4 directions
    if (search4ways(ws, reversed)) {
        found = 1;
    }

    // free memory allocated by the reversed_word
    free(reversed);

    return found;
}


int search4ways(WordSearch *ws, const char* word)
{

    int column, row;
    int found = 0;
    
    // multi-directional search
    // search vertically top to bottom
    if (searchVertical(ws, word, &row, &column)) {
        found = 1;
        // capitalize
        convertToUpper(ws, row, column, strlen(word), DOWN, NONE);
    }

    // search horizontally left to right
    if (searchHorizontal(ws, word, &row, &column)) {
        found = 1;
        // capitalize
        convertToUpper(ws, row, column, strlen(word), NONE, RIGHT);
    }

    // search diagonally top-left to bottom-right
    if (searchDiag1(ws, word, &row, &column)) {
        found = 1;
        // capitalize
        convertToUpper(ws, row, column, strlen(word), DOWN, RIGHT);
    }

    // search diagonally top-right to bottom-left
    if (searchDiag2(ws, word, &row, &column)) {
        found = 1;
        // capitalize
        convertToUpper(ws, row, column, strlen(word), DOWN, LEFT);
    }

    return found;
}


int searchVertical(WordSearch *ws, const char* word, int* row, int* column)
{

    int wordLength = strlen(word);

    for (int r = 0; r <= ws->rowNum - wordLength; r++) {
        for (int c = 0; c < ws->colNum; c++) {
            int equal = 1;
            for (int i = 0; i < wordLength && equal; i++) {
                equal = (tolower(ws->grid[r + i][c]) == word[i]);
            }
            if (equal) {
                // word found, save position
                *row = r;
                *column = c;
                return 1;
            }
        }
    }
    return 0; // word not found
}




int searchHorizontal(WordSearch *ws, const char* word, int* row, int* column)
{

    int wordLength = strlen(word);
    
    for (int r = 0; r < ws->rowNum; r++) {
        for (int c = 0; c <= ws->colNum - wordLength; c++) {
            int equal = 1;
            for (int i = 0; i < wordLength && equal; i++) {
                equal = (tolower(ws->grid[r][c + i]) == word[i]);
            }
            if (equal) {
                // word found, save position
                *row = r;
                *column = c;
                return 1;
            }
        }
    }
    return 0; // word not found
}




int searchDiag1(WordSearch *ws, const char* word, int* row, int* column)
{

   int wordLength = strlen(word);

    for (int r = 0; r <= ws->rowNum - wordLength; r++) {
        for (int c = 0; c <= ws->colNum - wordLength; c++) {
            int equal = 1;
            for (int i = 0; i < wordLength && equal; i++) {
                equal = tolower(ws->grid[r + i][c + i]) == word[i];
            }
            if (equal) {
                // word found, save position
                *row = r;
                *column = c;
                return 1;
            }
        }
    }
    return 0; // word not found
}



int searchDiag2(WordSearch *ws, const char* word, int* row, int* column)
{

    int wordLength = strlen(word);

    for (int r = 0; r <= ws->rowNum - wordLength; r++) {
        for (int c = wordLength - 1; c < ws->colNum; c++) {
            int equal = 1;
            for (int i = 0; i < wordLength && equal; i++) {
                equal = tolower(ws->grid[r + i][c - i]) == word[i];
            }
            if (equal) {
                // word found, save position
                *row = r;
                *column = c;
                return 1;
            }
        }
    }
    return 0; // word not found
}


void makeLower(char* word)
{

    int i = 0;
    while (word[i] != '\0') {
        word[i] = tolower(word[i]);
        i++;
    }
}



void convertToUpper(WordSearch *ws, int row, int column, int length, int nextRow, int nextCol)
{

    int r = row;
    int c = column;
    for (int i = 0; i < length; i++) {
        // check that position is within grid limits
        if (r >= 0 && r < ws->rowNum && c >= 0 && c < ws->colNum) {
            ws->grid[r][c] = toupper(ws->grid[r][c]);
            // advance (r,c) to the next character
            r += nextRow;
            c += nextCol;
        }
    }
}



void reverseWord (char* word)
{

    int i = 0;
    int j = strlen(word) - 1;
    while (i < j) {
        char temp = word[i];
        word[i] = word[j];
        word[j] = temp;
        i++;
        j--;
    }
}
