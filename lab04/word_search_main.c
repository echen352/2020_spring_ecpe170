// Ellis Chen
// ellischen352@gmail.com

// word_search_main.c
// main program

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "word_search.h"

#define MAX_LINE 250

int main()
{
    char fileName[MAX_LINE];
    char word[MAX_LINE];
    WordSearch puzzle;
    int wordsToFind;

    //prompt user for file name
    printf("Crossword Puzzle Game\n");
    printf("Enter game file name to load: ");
    scanf("%s", fileName);

    // load puzzle
    if (!load(fileName, &puzzle, &wordsToFind)) {
        // free memory
        clearPuzzle(&puzzle);
        return 1;
    }

    int exit = 0;
    int wordsFound = 0;
    
    while (!exit) {
        // print current puzzle
        printf("\n\n");
        printPuzzle(&puzzle);

        // prompt for word
        printf("\nPlease enter your word: ");
        scanf("%s", word);

        // convert to lower case
        makeLower(word);

        // check if all words are found
        if (wordsFound == wordsToFind) {
            // search and print result
            if (searchWord(&puzzle, word)) {
            	wordsFound += searchWord(&puzzle, word);
                printf("\nWord %s found!\n", word);
                }
            else
                printf("\nWord %s NOT found!\n", word);
        } else {
            exit = 1;
        }
    }

    // free memory
    clearPuzzle(&puzzle);

    return 0;
}
