// Ellis Chen
// ellischen352@gmail.com

// word_search.h
// Function prototypes

#ifndef WORD_SEARCH_H
#define WORD_SEARCH_H

// directions on the grid
#define UP -1
#define DOWN 1
#define NONE 0
#define LEFT -1
#define RIGHT 1

typedef struct WordSearch {
    int rowNum;
    int colNum;
    int numWords;
    char **grid;
} WordSearch;

// loadPuzzle: loads puzzle from the text file
// returns 1 if puzzle loaded, 0 if load failed
   
int load(const char* fileName, WordSearch *ws, int wordsToFind);

// printPuzzle: displays puzzle to the stdout
// WordSearch *ws: puzzle to be printed

void printPuzzle(WordSearch *ws);

//clearPuzzle: frees memory allocated by the puzzle grid

void clearPuzzle(WordSearch *ws);

/* searchWord: searches for the word in all directions
   returns 1 if found and capitalizes its letters
   returns 0 if not found
*/
int searchWord(WordSearch *ws, const char* word);

/* search4ways: searches for the word in 4 directions (only direct word, not reversed)
   returns 1 if found and capitalizes its letters
   returns 0 if not found
*/
int search4ways(WordSearch *ws, const char* word);

/* searchVertical: searches for the word vertically
   returns 1 if found and saves position of the first letter at (row, column)
   returns 0 if not found
*/
int searchVertical(WordSearch *ws, const char* word, int* row, int* column);

/* searchHorizontal: searches for the word horizontally
   returns 1 if found and saves position of the first letter at (row, column)
   returns 0 if not found
*/
int searchHorizontal(WordSearch *ws, const char* word, int* row, int* column);

/* searchDiag1: searches for the word diagonally top-left to bottom-right
   returns 1 if found and saves position of the first letter at (row, column)
   returns 0 if not found
*/
int searchDiag1(WordSearch *ws, const char* word, int* row, int* column);

/* searchDiag2: searches for the word diagonally from top-right to bottom-left
   returns 1 if found and saves position of the first letter at (row, column)
   returns 0 if not found
*/
int searchDiag2(WordSearch *ws, const char* word, int* row, int* column);

// reverseWord: reverses letters of words
void reverseWord(char* word);

// makeLower: converts all letters of the word to lowercase */
void makeLower(char* word);

/* convertToUpper: capitalizes letters
   row,column: position of the first letter
   length: number of characters to convert
   Nextrow: direction of row (-1 left, 0-vertical, 1 right)
   nextCol: direction of column (-1 up, 0-horizontal, 1 down)
*/
void convertToUpper(WordSearch *ws, int row, int column, int length, int nextRow, int nextCol);

#endif // WORD_SEARCH_H

