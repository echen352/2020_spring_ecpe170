#!/usr/bin/env python3

# Python DNS query client
#
# Example usage:
#   ./dns.py --type=A --name=www.pacific.edu --server=8.8.8.8
#   ./dns.py --type=AAAA --name=www.google.com --server=8.8.8.8

# Should provide equivalent results to:
#   dig www.pacific.edu A @8.8.8.8 +noedns
#   dig www.google.com AAAA @8.8.8.8 +noedns
#   (note that the +noedns option is used to disable the pseduo-OPT
#    header that dig adds. Our Python DNS client does not need
#    to produce that optional, more modern header)


from dns_tools import dns  # Custom module for boilerplate code

import argparse
import ctypes
import random
import socket
import struct
import sys

def main():

    # Setup configuration
    parser = argparse.ArgumentParser(description='DNS client for ECPE 170')
    parser.add_argument('--type', action='store', dest='qtype',
                        required=True, help='Query Type (A or AAAA)')
    parser.add_argument('--name', action='store', dest='qname',
                        required=True, help='Query Name')
    parser.add_argument('--server', action='store', dest='server_ip',
                        required=True, help='DNS Server IP')

    args = parser.parse_args()
    qtype = args.qtype
    qname = args.qname
    server_ip = args.server_ip
    port = 53
    server_address = (server_ip, port)

    if qtype not in ("A", "AAAA"):
        print("Error: Query Type must be 'A' (IPv4) or 'AAAA' (IPv6)")
        sys.exit()

    # Create UDP socket
    # ---------
    # STUDENT TO-DO
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # ---------

    # Generate DNS request message
    # ---------
    # STUDENT TO-DO
    msg = bytes([])
    id = bytes([random.randint(0, 255), random.randint(0, 255)])
    flg = bytes([0x01, 0x20])
    msg = msg + id
    msg = msg + flg
    msg = msg + bytes([0x00, 0x01])
    for i in range(0,3):
    	msg = msg + bytes([0x00, 0x00])
    q = qname.split(".")
    for i in range(len(q)):
    	msg = msg + bytes([len(q[i])])
    	msg = msg + bytes(q[i], "utf-8")
    msg = msg + bytes([0x00])
    if (qtype == "A"):
    	msg = msg + bytes([0x00, 0x01])
    elif (qtype == "AAAA"):
    	msg = msg + bytes([0x00, 0x1c])
    msg = msg + bytes([0x00, 0x01])
    # ---------


    # Send request message to server
    # (Tip: Use sendto() function for UDP)
    # ---------
    # STUDENT TO-DO
    s.sendto(msg, server_address)
    # ---------


    # Receive message from server
    # (Tip: use recvfrom() function for UDP)
    # ---------
    # STUDENT TO-DO
    raw_bytes, _ = s.recvfrom(512)
    # ---------


    # Close socket
    # ---------
    # STUDENT TO-DO
    s.close()
    # ---------


    # Decode DNS message and display to screen
    dns.decode_dns(raw_bytes)


if __name__ == "__main__":
    sys.exit(main())
