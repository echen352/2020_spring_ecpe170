.globl main
.text
main:
# initializes A,B,C,D,E,F with constants into registors
li $s0, 15 #A=15
li $s1, 10 #B=10
li $s2, 7 #C=7
li $s3, 2 #D=2
li $s4, 18 #E=18
li $s5, -3 #F=-3
lw $s0, Z #loads Z from memory

#addition and subtraction of variables
add $t0, $s0, $s1 #A+B
sub $t1, $s2, $s3 #C-D
add $t0, $t0, $t1 #(A+B)+(C-D)
add $t1, $s4, $s5 #E+F
add $t0, $t0, $t1 #(A+B)+(C-D)+(E+F)
sub $t1, $s0, $s2 #A-C
sub $s0, $t0, $t1 #(A+B)+(C-D)+(E+F)-(A-C)

sw $s0, Z # stores answer into memory Z
li $v0, 10 # exit
syscall
.data
Z: .word 0 # memory variable Z
