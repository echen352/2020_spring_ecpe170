.globl main
.text
main:
# registor map
# $t0 variable i
# $t1 result pointer
# $t2 stores offset of user string
# $t3 stores string[i]
# $t4 stores the byte, e.g. 'e'
# $t5 stores the byte that result points to
# $s0 user string

li $t0, 0 # i=0
li $t1, 0 # *result=NULL

# system call to print string "Enter a string: "
li $v0, 4 # print string
la $a0, msg1 # address of string
syscall

# system call to scan user input string
li $v0, 8 # take in input string
la $a0, string # address of string
syscall
move $s0, $a0 # move string to a better registor

lb $t4, letter # assign 'e' to registor

# the while loop to check for characters in string
while: add $t2, $s0, $t0 # calculates the offset, string+i
       lb $t3, 0($t2) # store string[i]
       beqz $t3, outwhile # breaks out of while loop when string[i]=='\0'
       j if # jumps to if statement when string[i]!='\0'
       
# 1st if statement to compare letter to string
if: beq $t3, $t4, assign # if string[i]=='e' is true, save address to result pointer
    j iter # else skip if statement
    
# saves the current address of matching byte into the result pointer
assign: addi $t1, $t2, 0 # result=&string[i]
        j outwhile # break out of while loop

# iterate i
iter: addi $t0, $t0, 1 # i++
      j while # execute while loop

# 2nd if statement to verify that result pointer is not NULL
outwhile: bne $t1, 0, otherif # if result!=NULL, prints the address and character of result
          j else

# system call to print results
otherif: li $v0, 4 # print string "First match at address "
         la $a0, msg2 # address of string
         syscall
         
         li $v0, 1 # prints address of result
         move $a0, $t1 # address of result
         syscall
         
         li $v0, 4 # prints new line
         la $a0, newline # address of new line
         syscall
         
         li $v0, 4 # print string "The matching character is "
         la $a0, msg3 # address of string
         syscall
         
         lb $t5, 0($t1) # saves the byte that result points to
         li $v0, 11 # print the byte that result points to
         move $a0, $t5 # address of byte
         syscall
         
         li $v0, 4 # prints new line
         la $a0, newline # address of new line
         syscall
         
         j end # end

# else statement for when result points to NULL
else: li $v0, 4 # print string "No match found"
      la $a0, msg4 # address of string
      syscall
      
      li $v0, 4 # prints new line
      la $a0, newline # address of new line
      syscall

end:
li $v0, 10
syscall
.data
string: .space 256 # user input string
letter: .byte 'e' # the character we want to find
msg1: .asciiz "Enter a string: " # string 1
msg2: .asciiz "First match at address " # string 2
msg3: .asciiz "The matching character is " # string 3
msg4: .asciiz "No match found" # string 4
newline: .asciiz "\n" # new line
