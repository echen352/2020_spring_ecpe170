.globl main
.text
main:
# registor map
# $s0 base address for A
# $s1 base address for B
# $s2 variable C
# $t0 variable i
# $t1 stores offset
# $t2 store A[i]
# $t3 store B[i]

la $s0, A # loads A[5]
la $s1, B # loads B[5]
lw $s2, C # loads C
li $t0, 0 # i=0

# initializes B[5]
li $t3, 1 # 1
sw $t3, 0($s1) # B[0]=1
li $t3, 2 # 2
sw $t3, 4($s1) # B[1]=2
li $t3, 3 # 3
sw $t3, 8($s1) # B[2]=3
li $t3, 4 # 4
sw $t3, 12($s1) # B[3]=4
li $t3, 5 # 5
sw $t3, 16($s1) # B[4]=5

# executes the for loop
for: blt $t0, 5, loop # executes loop for i<5
     j outfor # break out of loop
loop: add $t1, $t0, $t0 # 2*i
      add $t1, $t1, $t1 # 4*i
      add $t1, $s1, $t1 # offset
      lw $t3, 0($t1) # loads B[i]
      add $t2, $t3, $s2 # B[i]+C
      sw $t2, 0($t1) # stores A[i]=B[i]+C
      addi $t0, $t0, 1 # i++
      j for # for loop
      
outfor: addi $t0, $t0, -1 # i--

# executes the while loop
while: blt $t0, 0, end # break if i<0
       add $t1, $t0, $t0 # 2*i
       add $t1, $t1, $t1 # 4*i
       add $t1, $s0, $t1 # offset
       lw $t2, 0($t1) # load A[i]
       add $t2, $t2, $t2 # A[i]*2
       sw $t2, 0($t1) # store A[i]=A[i]*2
       addi $t0, $t0, -1 # i--
       j while # while loop
end:
li $v0, 10
syscall
.data
A: .space 20 # array that holds 5 integer elements
B: .space 20 # array that holds 5 integer elements
C: .word 12 # variable that contains the value 12
