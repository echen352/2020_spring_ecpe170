.globl main
.text
main:
# loads memory into registors
lw $s0, A # loads A
lw $s1, B # loads B
lw $s2, C # loads C
lw $s3, Z # loads Z

# if statement block
bgt $s0, $s1, iftrue # checks if A>B is true
blt $s2, 5, iftrue # checks if c<5 is true
bgt $s0, $s1, conditional # A>B is true, now check (C+1)==7
j else # A>B is false
conditional: addi $t0, $s2, 1 # stores C+1
    beq $t0, 7, elseiftrue # checks if (C+1)==7 is true
    j else # (C=1)==7 is false
iftrue: li $s3, 1 # Z=1
        j switch # go to switch statement
elseiftrue: li $s3, 2 # Z=2
            j switch # go to switch statement
else: li $s3, 3 # Z=3

# switch statement block
switch: beq $s3, 1, case1 # if Z=1, go to case1
        beq $s3, 2, case2 # if Z=2, go to case2
        li $s3, 0 # Z=0
        j end # break
case1: li $s3, -1 # Z=-1
       j end # break
case2: addi $s3, $s3, 2 # Z-=-2
       j end # break
       
end: sw $s3, Z # store Z into memory
li $v0, 10
syscall
.data
A: .word 10 # memory variable A
B: .word 15 # memory variable B
C: .word 6 # memory variable C
Z: .word 0 # memory variable Z
