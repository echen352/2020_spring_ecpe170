.globl main
.text
main:
# loads memory into registors
lw $s0, Z # loads Z
lw $s1, i # loads i
li $s1, 0 # i=0

while: bgt $s1, 20, dowhile # break out of loop
       addi $s0, $s0, 1 # Z++
       addi $s1, $s1, 2 # i+=2
       j while # while loop is always true
       
dowhile: addi $s0, $s0, 1 # Z++
         blt $s0, 100, dowhile # while Z<100
         j secondwhile # break
         
secondwhile: ble $s1, 0, end # break if i<=0
             addi $s0, $s0, -1 # Z--
             addi $s1, $s1, -1 # i--
             j secondwhile # while loop
             
# saves registors into memory
end: sw $s0, Z # stores Z into memory
     sw $s1, i # stores i into memory
li $v0, 10
syscall
.data
Z: .word 2 # memory variable Z
i: .word 0 # memory variable i
