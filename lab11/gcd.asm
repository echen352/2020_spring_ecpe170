# Ellis Chen
# Lab11: MIPS Assembly Programming
.text			
.globl main		
main:
	# Register Map:
	# $s0 	i
	# $a0   low	
	# $a1 	hight  

	li $s0, 0 # i=0
forloop:
	blt $s0, 10, insidefor # if i<10 end for loop
	j endfor
	
insidefor:
	li $v0, 4 # print string 
	la $a0, msg1 # memory address of string "\nG.C.D of "
	syscall

	# Take first random integer n1	
	li $a0, 1 # function argument low=1
	li $a1, 10000 # function argument hight=10000	
	jal random_in_range # call random_in_range(low,high)
	# random_in_range returns a random integer 
						
	sw $v0, n1 # store random n1 in memory
	move $a0, $v0 # move random integer n1 to $a0
	li $v0, 1 # print integer
	syscall	
	
	li $v0, 4 # print string 
	la $a0, msg2 # memory address of string " and "
	syscall	

	# Take second random integer n2		
	li $a0, 1 # function argument low=1
	li $a1, 10000 # function argument high=10000	
	jal random_in_range # call random_in_range(low,high)
	# random_in_range returns a random integer
				
	sw $v0, n2 # store random n2 in memory	
	move $a0, $v0 # move random integer n2 to $a0
	li $v0, 1 # print integer
	syscall		
	
	li $v0, 4 # print string
	la $a0, msg3 # memory address of string " is "
	syscall
	
	# Calling gcd 
	lw $a1, n1 # function argument n1	
	lw $a2, n2 # function argument n2	
	
 	jal gcd	# call gcd(n1,n2)
 	# gcd returns an integer
 		
	move $a0,$v0 # move result GCD in $a0		
	
	li $v0, 1 # print integer GCD from $a0
	syscall		
	
	li $v0, 4 # print string syscall code = 4 
	la $a0, newLine	# memory address of newLine
	syscall		
	
	addi $s0, $s0,1	# i++
	j forloop # for loop
	
endfor:
	li $v0, 10 # return 0 
	syscall
	

#-----------------------------------------------------
# random_in_range(low,high)
# Gets a random integer within range
# $a0 low
# $a1 hight
# $v0 random positive integers

random_in_range:
	addi $sp, $sp, -4 # allocate space in stack
	sw $s1, 0($sp) # stores $s1
	addi $sp, $sp, -4 # allocate space in stack
	sw $s2, 0($sp) # stores $s2
	addi $sp, $sp, -4 # allocate space in stack
	sw $ra, 0($sp) # stores the return address

	move $s1, $a0 # load low
	move $s2, $a1 # load high	
	
	# range = high-low+1;	
	sub $s2, $s2, $s1 # high-low
	addi $s2, $s2, 1 # range=high-low+1

	jal get_random # generate random number
	
	# (rand_num % range) + low
	div $v0, $s2 # rand_num/range
	mfhi $v0 # rand_num%range
	addi $v0, $v0, 1 # (rand_num%range)+low
	
	lw $ra, 0($sp) # loads the return address
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s2, 0($sp) # loads $s2
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s1, 0($sp) # loads $s1
	addi $sp, $sp, 4 # deallocate space in stack
	
	jr $ra # jump to parent call

	
#-----------------------------------------------------
# get_random() 
# global variables m_z and m_w
# m_z = 36969 * (m_z & 65535) + (m_z >> 16);
# m_w = 18000 * (m_w & 65535) + (m_w >> 16);
# result = (m_z << 16) + m_w;  
# $v0 output result

get_random:
	addi $sp, $sp, -4 # allocate space in stack
	sw $s1, 0($sp) # stores $s1
	addi $sp, $sp, -4 # allocate space in stack
	sw $s2, 0($sp) # stores $s2
	addi $sp, $sp, -4 # allocate space in stack
	sw $s3, 0($sp) # stores $s3
	addi $sp, $sp, -4 # allocate space in stack
	sw $ra, 0($sp) # stores the return address
		
	li $s3, 36969 # load immediate value 36969
	lw $s1, m_z # load m_z in $s1
	and $s2, $s1, 65535 # $s1=(m_z & 65535)
	mult $s3, $s2 # 36969*(m_z & 65535)
	mflo $v0 # $v0=36969*(m_z & 65535)
	
	srl $s1, $s1, 16 # $s0=(m_z >> 16) 
	add $v0, $v0, $s1 # $v0=36969*(m_z & 65535)+(m_z >> 16)
	
	sw $v0, m_z # save new value m_z in memory
	
	li $s3, 18000 # Load immediate value 18000
	lw $s1, m_w # load m_w in $s0
	and $s2, $s1, 65535 # $s1=(m_w & 65535)
	mult $s3, $s2 # 18000*(m_w & 65535)
	mflo $s2 # $s1=18000*(m_w & 65535)	

	srl $s1, $s1, 16 # $s0=m_w >> 16	
	add $s1, $s1, $s2 # $s0=18000*(m_w & 65535)+(m_w >> 16)	
	
	sw $s1, m_w # save new value m_w in memory
	
	# result=(m_z << 16)+m_w
	sll $v0, $v0, 16 # (m_z << 16)
	addu $v0, $v0, $s1 # (m_z << 16)+m_w	

	# result in $v0 	
	blt $v0, $zero, makepos	# if result < 0 go to makepos
	j end_rand # else go to end_rand
	
makepos:
 	neg $v0, $v0 #result should be a positive integer
	addi $v0, $v0, 1 # result = -result
 
end_rand:
	lw $ra, 0($sp) # stores the return address
 	addi $sp, $sp, 4 # deallocate space in stack
 	lw $s3, 0($sp) # stores $s3
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s2, 0($sp) # stores $s2
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s1, 0($sp) # stores $s1
	addi $sp, $sp, 4 # deallocate space in stack
	
	jr $ra # return result

	

#************************************************
# gcd(n1,n2)
# returns gcd
# $a0 = n1, $a1 = n2
# $v0 = gcd

gcd:
	addi $sp, $sp, -4 # allocate space in stack
	sw $s1, 0($sp) # stores $s1
	addi $sp, $sp, -4 # allocate space in stack
	sw $s2, 0($sp) # stores $s2
	addi $sp, $sp, -4 # allocate space in stack
	sw $ra, 0($sp) # stores the return address
	
	bne $a2, $zero, continue # if n2 != 0 go to continue
	move $v0, $a1 # else return n1
	
	lw $ra, 0($sp) # loads the return address
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s2, 0($sp) # loads $s1
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s1, 0($sp) # loads $s2
	addi $sp, $sp, 4 # deallocate space in stack
	
	jr $ra # return n1
  
continue:
	div $a1, $a2 # n1/n2; remainder
	mfhi $s1 # n1%n2
	move $s2, $s1 # store remainder in $s2

	bne $s1, $zero, next # if remainder($s0 != 0) goto next 
	add $v0, $a2, $zero # else n2 = result, move in $v0
	
	jr $ra # return result in $v0
  
next:
	add $a1, $a2, $zero # n1=n2
	add $s1, $zero, $s2 # load remainder in $s1
	add $a2, $s1, $zero # n2=remainder
	jal gcd # recursive call

	# Exit Recursion
	add $v0, $zero, $a2
	lw $ra, 0($sp) # loads the return address
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s2, 0($sp) # loads $s1
	addi $sp, $sp, 4 # deallocate space in stack
	lw $s1, 0($sp) # loads $s2
	addi $sp, $sp, 4 # deallocate space in stack
	
	jr $ra # return

.data
msg1: .asciiz "\nG.C.D. of "
msg2: .asciiz " and "
msg3: .asciiz " is "
newLine: .asciiz ".\n "
 
m_w: .word 50000
m_z: .word 60000

n1: .word ? # 1st random integer
n2: .word ? # 2nd random integer
